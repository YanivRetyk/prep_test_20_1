import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent } from './books/books.component';


import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    { path: 'books', component: BooksComponent },
    { path: '',
      redirectTo: '/books',
      pathMatch: 'full'
    },
  ];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule,
    RouterModule.forRoot(
            appRoutes,
            { enableTracing: true } // <-- debugging purposes only
          ) 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
